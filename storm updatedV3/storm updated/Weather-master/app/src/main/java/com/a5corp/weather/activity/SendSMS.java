package com.a5corp.weather.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;

import com.a5corp.weather.R;
import com.a5corp.weather.fragment.GraphsFragment;
import com.a5corp.weather.fragment.MapsFragment;
import com.a5corp.weather.fragment.WeatherFragment;
import com.a5corp.weather.model.WeatherFort;
import com.a5corp.weather.permissions.GPSTracker;
import com.a5corp.weather.preferences.Prefs;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.weather_icons_typeface_library.WeatherIcons;


import java.sql.Time;
import java.util.ArrayList;


import static com.a5corp.weather.activity.WeatherActivity.newGraphInstance;



public class SendSMS extends AppCompatActivity {
    Prefs preferences;
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS=0 ;
    Button sendBtn;
    WeatherFragment wf;
    Toolbar toolbar;
    Drawer drawer;
    int click = 0;
    CountDownTimer cTimer = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sendsms1);
        preferences = new Prefs(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initDrawer();
        sendBtn = (Button)findViewById(R.id.btnSendSMS);

        

        sendBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GPSTracker gps = new GPSTracker(getApplicationContext());
                String lat = gps.getLatitude();
                String lon = gps.getLongitude();
                sendSMSMessage("911", "HELP ME!! I'm at this location" + "Latitude: " +  lat +" "+"Longitude: " + lon);
                click++;

                if(click == 3)
                {
                    dialogbox();
                    startTimer();
                    click=0;
                }
            }

        });
    }

    void startTimer() {
        cTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                sendBtn.setEnabled(true);
            }
        };
        cTimer.start();
    }

    public void dialogbox()
    {
        new AlertDialog.Builder(this)
                .setMessage("Send request exceed")
                .setCancelable(false)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sendBtn = (Button)findViewById(R.id.btnSendSMS);
                        sendBtn.setEnabled(false);
                    }
                })
                .show();
    }

    public void initDrawer() {
        final Context context = this;
        final IProfile profile = new ProfileDrawerItem().withName("STORM")
                .withIcon(R.drawable.androoidlogo);
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)

                //   .withHeaderBackground(R.drawable.header)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .addProfiles(
                        profile
                )
                .withSelectionListEnabled(false)
                .withProfileImagesClickable(false)
                .build();
        SecondaryDrawerItem item1 = new SecondaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_home)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(WeatherIcons.Icon.wic_day_sunny));
        SecondaryDrawerItem item5 = new SecondaryDrawerItem().withIdentifier(5).withName("Evacuation Center")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_map));
        SecondaryDrawerItem item6 = new SecondaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_about)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_book));
        SecondaryDrawerItem item7 = new SecondaryDrawerItem().withIdentifier(7).withName("Safety Precautions")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_save));
        SecondaryDrawerItem item8 = new SecondaryDrawerItem().withIdentifier(8).withName("Flood Risk")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_save));
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(2).withName("Weather Map")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_create))
                .withSelectable(false);
        SecondaryDrawerItem item3 = new SecondaryDrawerItem().withIdentifier(3).withName("Weather Graph")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_trending_up))
                .withSelectable(false);
        SecondaryDrawerItem item4 = new SecondaryDrawerItem().withIdentifier(4).withName("S.O.S EMERGENCY")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_warning))
                .withSelectable(false);
        drawer = new DrawerBuilder()
                .withSliderBackgroundColor(ContextCompat.getColor(this , R.color.md_grey_900))
                .withActivity(this)
                .withToolbar(toolbar)
                .withSelectedItem(4)
                .withTranslucentStatusBar(true)
                .withAccountHeader(headerResult)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        item4,
                        item1,
                        item5,
                        item7,
                        item2,
                        item3,
                        item8,
                        item6

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        if (drawerItem != null) {
                            switch((int) drawerItem.getIdentifier()) {
                                case 1:
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment, new WeatherFragment())
                                            .commit();
                                    break;

                                case 2:
                                    MapsFragment mapsFragment = new MapsFragment();
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment,mapsFragment)
                                            .commit();
                                    break;
                                case 3:
                                    GraphsFragment graphsFragment = newGraphInstance(new ArrayList<WeatherFort.WeatherList>(wf.getDailyJson()));
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment,graphsFragment)
                                            .commit();

                                    break;
                                case 4:
                                    startActivity(new Intent(SendSMS.this, SendSMS.class));
                                    break;
                                case 5:
                                    startActivity(new Intent(SendSMS.this, MapsActivity.class));
                                    break;
                                case 6:
                                    startActivity(new Intent(SendSMS.this, About.class));
                                    break;
                                case 7:
                                    startActivity(new Intent(SendSMS.this, Safety.class));
                                    break;
                                case 8:
                                    startActivity(new Intent(SendSMS.this, image.class));
                                    break;
                            }
                        }
                        return false;
                    }
                })
                .build();
    }

    protected void sendSMSMessage(String phoneNumber, String message) {
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.SEND_SMS))
            {

            }
            else
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        }
        else
        {

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, message, null, null);
        }
    }
}


