package com.a5corp.weather.activity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("Longitude")
    @Expose
    private Double longitude;
    @SerializedName("Latitude")
    @Expose
    private Double latitude;

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;

    @SerializedName("EvacuationCenterID")
    @Expose
    private Integer evacuationCenterID;
    @SerializedName("EvacName")
    @Expose
    private String evacName;
    @SerializedName("EvacDescription")
    @Expose
    private String evacDescription;

    @SerializedName("CurrentNumber")
    @Expose
    private Integer currentNumber;
    @SerializedName("Capacity")
    @Expose
    private Integer capacity;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("CategoryType")
    @Expose
    private Object categoryType;


    /**
     *
     * @return
     * The geometry
     */
    public Geometry getGeometry() {
        return geometry;
    }

    /**
     *
     * @param geometry
     * The geometry
     */
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Integer getEvacuationCenterID() {
        return evacuationCenterID;
    }

    public void setEvacuationCenterID(Integer evacuationCenterID) {
        this.evacuationCenterID = evacuationCenterID;
    }

    public String getEvacName() {
        return evacName;
    }

    public void setEvacName(String evacName) {
        this.evacName = evacName;
    }

    public String getEvacDescription() {
        return evacDescription;
    }

    public void setEvacDescription(String evacDescription) {
        this.evacDescription = evacDescription;
    }

    //get and set for long/lat were here

    public Integer getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(Integer currentNumber) {
        this.currentNumber = currentNumber;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Object categoryType) {
        this.categoryType = categoryType;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

}

