package com.a5corp.weather.activity;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by navneet on 17/7/16.
 */
public interface RetrofitMaps {

    /*
     * Retrofit get annotation with our URL
     * And our method that will return us details of student.
     */
    @GET("Api/EvacuationCenters")
    Call<List<Example>> GetEvacuationCenters();

}
