package com.a5corp.weather.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.a5corp.weather.R;
import com.a5corp.weather.fragment.GraphsFragment;
import com.a5corp.weather.fragment.MapsFragment;
import com.a5corp.weather.fragment.WeatherFragment;
import com.a5corp.weather.model.WeatherFort;
import com.a5corp.weather.preferences.Prefs;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.weather_icons_typeface_library.WeatherIcons;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class WeatherActivity extends AppCompatActivity {
    Prefs preferences;

    @BindView(R.id.fab) FloatingActionButton fab;
    @OnClick(R.id.fab) void fabClick() {
        fab.hide();
        showInputDialog();
    }
    WeatherFragment wf;
    Toolbar toolbar;
    Drawer drawer;
    NotificationManagerCompat mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mManager = NotificationManagerCompat.from(this);
        preferences = new Prefs(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        ButterKnife.bind(this);

        wf = new WeatherFragment();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, wf)
                    .commit();
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fab.hide();
        initDrawer();
    }

    public void hideFab() {
        fab.hide();
    }

    public void showFab() {
        fab.show();
    }

    public FloatingActionButton getFab() {
        return fab;
    }

    public void initDrawer() {
        final Context context = this;
        final IProfile profile = new ProfileDrawerItem().withName("STORM")
                .withIcon(R.drawable.androoidlogo);
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)

                //   .withHeaderBackground(R.drawable.header)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .addProfiles(
                        profile
                )
                .withSelectionListEnabled(false)
                .withProfileImagesClickable(false)
                .build();
        SecondaryDrawerItem item1 = new SecondaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_home)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(WeatherIcons.Icon.wic_day_sunny));
        SecondaryDrawerItem item5 = new SecondaryDrawerItem().withIdentifier(5).withName("Evacuation Center")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_map));
        SecondaryDrawerItem item6 = new SecondaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_about)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_book));
        SecondaryDrawerItem item7 = new SecondaryDrawerItem().withIdentifier(7).withName("Safety Precautions")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_save));
        SecondaryDrawerItem item8 = new SecondaryDrawerItem().withIdentifier(8).withName("Flood Risk")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_save));
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(2).withName("Weather Map")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_create))
                .withSelectable(false);
        SecondaryDrawerItem item3 = new SecondaryDrawerItem().withIdentifier(3).withName("Weather Graph")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_trending_up))
                .withSelectable(false);
        SecondaryDrawerItem item4 = new SecondaryDrawerItem().withIdentifier(4).withName("S.O.S EMERGENCY")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_warning))
                .withSelectable(false);
        drawer = new DrawerBuilder()
                .withSliderBackgroundColor(ContextCompat.getColor(this , R.color.md_grey_900))
                .withActivity(this)
                .withToolbar(toolbar)
                .withSelectedItem(1)
                .withTranslucentStatusBar(true)
                .withAccountHeader(headerResult)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        item4,
                        item1,
                        item5,
                        item7,
                        item2,
                        item3,
                        item8,
                        item6

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        if (drawerItem != null) {
                            switch((int) drawerItem.getIdentifier()) {
                                case 1:
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment, new WeatherFragment())
                                            .commit();
                                    break;

                                case 2:
                                    MapsFragment mapsFragment = new MapsFragment();
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment,mapsFragment)
                                            .commit();
                                    break;
                                case 3:
                                    GraphsFragment graphsFragment = newGraphInstance(new ArrayList<WeatherFort.WeatherList>(wf.getDailyJson()));
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment,graphsFragment)
                                            .commit();

                                    break;
                                case 4:
                                    startActivity(new Intent(WeatherActivity.this, SendSMS.class));
                                    break;
                                case 5:
                                    startActivity(new Intent(WeatherActivity.this, MapsActivity.class));
                                    break;
                                case 6:
                                    startActivity(new Intent(WeatherActivity.this, About.class));
                                    break;
                                case 7:
                                    startActivity(new Intent(WeatherActivity.this, Safety.class));
                                    break;
                                case 8:
                                    startActivity(new Intent(WeatherActivity.this, image.class));
                                    break;
                            }
                        }
                        return false;
                    }
                })
                .build();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        WeatherActivity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void showInputDialog() {
        new MaterialDialog.Builder(this)
                .title("Change City")
                .content("You can change the city by entering City name or the ZIP Code")
                .onAny(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        fab.show();
                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        fab.show();
                    }
                })
                .negativeText("CANCEL")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog , @NonNull DialogAction which) {
                        dialog.dismiss();
                        fab.show();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, @NonNull CharSequence input) {
                        changeCity(input.toString());
                        fab.show();
                    }
                }).show();
    }

    public void changeCity(String city){
        WeatherFragment wf = (WeatherFragment)getSupportFragmentManager()
                .findFragmentById(R.id.fragment);
        wf.changeCity(city);
        preferences.setCity(city);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private static final String DESCRIBABLE_KEY = "describable_key";

    public static GraphsFragment newGraphInstance(ArrayList<WeatherFort.WeatherList> describable) {
        GraphsFragment fragment = new GraphsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DESCRIBABLE_KEY, describable);
        fragment.setArguments(bundle);

        return fragment;
    }
}
