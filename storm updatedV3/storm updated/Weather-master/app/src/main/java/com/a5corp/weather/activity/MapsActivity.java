package com.a5corp.weather.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.a5corp.weather.R;
import com.a5corp.weather.fragment.GraphsFragment;
import com.a5corp.weather.fragment.MapsFragment;
import com.a5corp.weather.fragment.WeatherFragment;
import com.a5corp.weather.model.WeatherFort;
import com.a5corp.weather.preferences.Prefs;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.weather_icons_typeface_library.WeatherIcons;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import static com.a5corp.weather.activity.WeatherActivity.newGraphInstance;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    Prefs preferences;
    WeatherFragment wf;
    Toolbar toolbar;
    Drawer drawer;
    private GoogleMap mMap;
    double latitude;
    double longitude;
    private int PROXIMITY_RADIUS = 10000;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        preferences = new Prefs(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initDrawer();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        //show error dialog if Google Play Services not available
        if (!isGooglePlayServicesAvailable()) {
            Log.d("onCreate", "Google Play Services not available. Ending Test case.");
            finish();
        }
        else {
            Log.d("onCreate", "Google Play Services available. Continuing.");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void initDrawer() {
        final Context context = this;
        final IProfile profile = new ProfileDrawerItem().withName("STORM")
                .withIcon(R.drawable.androoidlogo);
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)

                //   .withHeaderBackground(R.drawable.header)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .addProfiles(
                        profile
                )
                .withSelectionListEnabled(false)
                .withProfileImagesClickable(false)
                .build();
        SecondaryDrawerItem item1 = new SecondaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_home)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(WeatherIcons.Icon.wic_day_sunny));
        SecondaryDrawerItem item5 = new SecondaryDrawerItem().withIdentifier(5).withName("Evacuation Center")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_map));
        SecondaryDrawerItem item6 = new SecondaryDrawerItem().withIdentifier(6).withName(R.string.drawer_item_about)
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_book));
        SecondaryDrawerItem item7 = new SecondaryDrawerItem().withIdentifier(7).withName("Safety Precautions")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_save));
        SecondaryDrawerItem item8 = new SecondaryDrawerItem().withIdentifier(8).withName("Flood Risk")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_save));
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(2).withName("Weather Map")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_create))
                .withSelectable(false);
        SecondaryDrawerItem item3 = new SecondaryDrawerItem().withIdentifier(3).withName("Weather Graph")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_trending_up))
                .withSelectable(false);
        SecondaryDrawerItem item4 = new SecondaryDrawerItem().withIdentifier(4).withName("S.O.S EMERGENCY")
                .withTextColor(ContextCompat.getColor(this , R.color.textColor))
                .withIcon(new IconicsDrawable(this)
                        .icon(GoogleMaterial.Icon.gmd_warning))
                .withSelectable(false);
        drawer = new DrawerBuilder()
                .withSliderBackgroundColor(ContextCompat.getColor(this , R.color.md_grey_900))
                .withActivity(this)
                .withToolbar(toolbar)
                .withSelectedItem(5)
                .withTranslucentStatusBar(true)
                .withAccountHeader(headerResult)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        item4,
                        item1,
                        item5,
                        item7,
                        item2,
                        item3,
                        item8,
                        item6

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        if (drawerItem != null) {
                            switch((int) drawerItem.getIdentifier()) {
                                case 1:
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment, new WeatherFragment())
                                            .commit();
                                    break;

                                case 2:
                                    MapsFragment mapsFragment = new MapsFragment();
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment,mapsFragment)
                                            .commit();
                                    break;
                                case 3:
                                    GraphsFragment graphsFragment = newGraphInstance(new ArrayList<WeatherFort.WeatherList>(wf.getDailyJson()));
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment,graphsFragment)
                                            .commit();

                                    break;
                                case 4:
                                    startActivity(new Intent(MapsActivity.this, SendSMS.class));
                                    break;
                                case 5:
                                    startActivity(new Intent(MapsActivity.this, MapsActivity.class));
                                    break;
                                case 6:
                                    startActivity(new Intent(MapsActivity.this, About.class));
                                    break;
                                case 7:
                                    startActivity(new Intent(MapsActivity.this, Safety.class));
                                    break;
                                case 8:
                                    startActivity(new Intent(MapsActivity.this, image.class));
                                    break;
                            }
                        }
                        return false;
                    }
                })
                .build();
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        build_retrofit_and_get_response();
    }

    //Parameter earlier: String type
    private void build_retrofit_and_get_response() {

        String url = "http://stormspaceapps.azurewebsites.net/";


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitMaps service = retrofit.create(RetrofitMaps.class);

        Call<List<Example>> call = service.GetEvacuationCenters();

        call.enqueue(new Callback<List<Example>>() {
            @Override
            public void onResponse(Response<List<Example>> response, Retrofit retrofit) {
                try {
                    mMap.clear();
                    // This loop will go through all the results and add marker on each location.
                    for (int i = 0; i < response.body().size(); i++) {



                        //Original properties
                        //Double lat = response.body().getResults().get(i).getGeometry().getLocation().getLatitude();
                        //Double lng = response.body().getResults().get(i).getGeometry().getLocation().getLongitude();

                        //My modified properties
                        Double lat = response.body().get(i).getLatitude();
                        Double lng = response.body().get(i).getLongitude();

                        //String placeName = response.body().getResults().get(i).getName();
                        //String vicinity = response.body().getResults().get(i).getVicinity();
                        MarkerOptions markerOptions = new MarkerOptions();
                        LatLng latLng = new LatLng(lat, lng);
                        // Position of Marker on Map
                        markerOptions.position(latLng);

                        // Adding Title to the Marker
                        //markerOptions.title(placeName + " : " + vicinity);
                        markerOptions.title(response.body().get(i).getEvacName());
                        markerOptions.snippet("Place: " + response.body().get(i).getEvacDescription() + "Capacity: " + response.body().get(i).getCapacity() +  "Occupants: " + response.body().get(i).getCurrentNumber() + "Type: " + response.body().get(i).getCategoryType());

                        // Adding Marker to the Camera.
                        Marker m = mMap.addMarker(markerOptions);
                        // Adding colour to the marker
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        // move map camera
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                    }
                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });

    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d("onLocationChanged", "entered");

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");

        // Adding colour to the marker
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));

        // Adding Marker to the Map
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f", latitude, longitude));

        Log.d("onLocationChanged", "Exit");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }
}
