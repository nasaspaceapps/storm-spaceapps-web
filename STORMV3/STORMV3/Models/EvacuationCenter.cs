﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STORMV3.Models
{
    public class EvacuationCenter
    {
     
        public virtual int EvacuationCenterID { get; set; }

      
        [Display(Name = "Center Name")]
        [MaxLength(50)]
        [DataType(DataType.Text)]
        public virtual string EvacName { get; set; }


        [Display(Name = "Center Category")]
        public virtual Category CategoryType { get; set; }


        [Display(Name = "Description")]
        [MaxLength(100)]
        [DataType(DataType.Text)]
        public virtual string EvacDescription { get; set; }

        
        [Display(Name = "Longitude")]
        public virtual float Longitude { get; set; }


        
        [Display(Name = "Latitude")]
        public virtual float Latitude { get; set; }

        
        [Display(Name = "Occupants")]

        public virtual int CurrentNumber { get; set; }

        public virtual int Capacity { get; set; }
        public virtual string Status { get; set; }


    }
}