﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STORMV3.Models
{
    public class VehicleDriver
    {
        public virtual int VehicleDriverID { get; set; }
        public virtual int DriverID { get; set; }
        public virtual int ReliefGoodVehicleID { get; set; }

        public virtual Driver Driver { get; set; }


        public virtual ReliefGoodVehicle ReliefGoodVehicle { get; set; }
    }
}