﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STORMV3.Models
{
    public class Location
    {
        public virtual int LocationID { get; set; }

    
        public virtual float Longitude { get; set; }
    
        public virtual float Latitude { get; set; }
   
        public virtual ReliefGoodVehicle VehicleDriver { get; set; }
     
        public virtual DateTime TimeStamp { get; set; }
    }
}