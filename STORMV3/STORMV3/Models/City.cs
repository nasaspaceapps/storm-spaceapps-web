﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STORMV3.Models
{
    public class City
    {
        public virtual int CityID { get; set; }

        public virtual string CityName { get; set; }

        public virtual string Country { get; set; }

        public virtual float Latitude { get; set; }

        public virtual float Longitude { get; set; }
    }
}