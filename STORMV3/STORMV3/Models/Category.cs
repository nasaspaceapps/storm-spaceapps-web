﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STORMV3.Models
{
    public class Category
    {
        public virtual int CategoryID { get; set; }
   
        [MaxLength(30)]
        [DataType(DataType.Text)]
        [Display(Name = "Evacuation Center Type")]
        public virtual string CategoryType { get; set; }
    }
}