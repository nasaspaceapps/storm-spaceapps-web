﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STORMV3.Models
{
   public class Driver
    {
       public virtual int DriverID { get; set; }

     
        [MaxLength(30)]
        [DataType(DataType.Text)]
        [Display(Name = "FirstName")]
       public virtual string DriverFirstName { get; set; }

     
        [MaxLength(30)]
        [DataType(DataType.Text)]
        [Display(Name = "LasttName")]
       public virtual string LasttName { get; set; }

   
        [MaxLength(30)]
        [DataType(DataType.Text)]
        [Display(Name = "MiddelName")]
       public virtual string MiddleName { get; set; }

  
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "PhoneNumber")]
       public virtual string MobileNumber { get; set; }

       
        [Display(Name = "Driver Gender")]
       public virtual string Gender { get; set; }







    }
}