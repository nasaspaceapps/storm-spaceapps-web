﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STORMV3.Models
{
    public class FloodArea
    {

        public virtual int FloodAreaId { get; set; }
        public virtual City City { get; set; }


        public virtual string Tittle { get; set; }

        public virtual string descripton { get; set; }

        public virtual DateTime Year { get; set; }

        public virtual string FloodLevel { get; set; }
    }
}