﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace STORMV3.Models
{
    public class DeliveryLocation
    {
        public virtual int DeliveryLocationID { get; set; }
     
        public virtual VehicleDriver VehicleDriver { get; set; }
  
        public virtual EvacuationCenter EvacuationCenter { get; set; }

     
        public virtual DateTime DateOfDispatch { get; set; }
   
        public virtual DateTime EstimateDateOfArrival { get; set; }
    }
}