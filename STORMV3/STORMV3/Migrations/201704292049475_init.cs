namespace STORMV3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID", "dbo.EvacuationCenters");
            DropForeignKey("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID", "dbo.VehicleDrivers");
            DropForeignKey("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID", "dbo.ReliefGoodVehicles");
            DropForeignKey("dbo.VehicleDrivers", "DriverName_DriverID", "dbo.Drivers");
            DropForeignKey("dbo.VehicleDrivers", "PlateNumber_ReliefGoodVehicleID", "dbo.ReliefGoodVehicles");
            DropIndex("dbo.DeliveryLocations", new[] { "EvacuationCenter_EvacuationCenterID" });
            DropIndex("dbo.DeliveryLocations", new[] { "VehicleDriver_VehicleDriverID" });
            DropIndex("dbo.VehicleDrivers", new[] { "DriverName_DriverID" });
            DropIndex("dbo.VehicleDrivers", new[] { "PlateNumber_ReliefGoodVehicleID" });
            DropIndex("dbo.Locations", new[] { "VehicleDriver_ReliefGoodVehicleID" });
            RenameColumn(table: "dbo.VehicleDrivers", name: "DriverName_DriverID", newName: "DriverID");
            RenameColumn(table: "dbo.VehicleDrivers", name: "PlateNumber_ReliefGoodVehicleID", newName: "ReliefGoodVehicleID");
            AlterColumn("dbo.Categories", "CategoryType", c => c.String(maxLength: 30));
            AlterColumn("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID", c => c.Int());
            AlterColumn("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID", c => c.Int());
            AlterColumn("dbo.EvacuationCenters", "EvacName", c => c.String(maxLength: 50));
            AlterColumn("dbo.EvacuationCenters", "EvacDescription", c => c.String(maxLength: 100));
            AlterColumn("dbo.VehicleDrivers", "DriverID", c => c.Int(nullable: false));
            AlterColumn("dbo.VehicleDrivers", "ReliefGoodVehicleID", c => c.Int(nullable: false));
            AlterColumn("dbo.Drivers", "DriverFirstName", c => c.String(maxLength: 30));
            AlterColumn("dbo.Drivers", "LasttName", c => c.String(maxLength: 30));
            AlterColumn("dbo.Drivers", "MiddleName", c => c.String(maxLength: 30));
            AlterColumn("dbo.Drivers", "MobileNumber", c => c.String());
            AlterColumn("dbo.Drivers", "Gender", c => c.String());
            AlterColumn("dbo.ReliefGoodVehicles", "PlateNumber", c => c.String());
            AlterColumn("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID", c => c.Int());
            CreateIndex("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID");
            CreateIndex("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID");
            CreateIndex("dbo.VehicleDrivers", "DriverID");
            CreateIndex("dbo.VehicleDrivers", "ReliefGoodVehicleID");
            CreateIndex("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID");
            AddForeignKey("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID", "dbo.EvacuationCenters", "EvacuationCenterID");
            AddForeignKey("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID", "dbo.VehicleDrivers", "VehicleDriverID");
            AddForeignKey("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID", "dbo.ReliefGoodVehicles", "ReliefGoodVehicleID");
            AddForeignKey("dbo.VehicleDrivers", "DriverID", "dbo.Drivers", "DriverID", cascadeDelete: true);
            AddForeignKey("dbo.VehicleDrivers", "ReliefGoodVehicleID", "dbo.ReliefGoodVehicles", "ReliefGoodVehicleID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VehicleDrivers", "ReliefGoodVehicleID", "dbo.ReliefGoodVehicles");
            DropForeignKey("dbo.VehicleDrivers", "DriverID", "dbo.Drivers");
            DropForeignKey("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID", "dbo.ReliefGoodVehicles");
            DropForeignKey("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID", "dbo.VehicleDrivers");
            DropForeignKey("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID", "dbo.EvacuationCenters");
            DropIndex("dbo.Locations", new[] { "VehicleDriver_ReliefGoodVehicleID" });
            DropIndex("dbo.VehicleDrivers", new[] { "ReliefGoodVehicleID" });
            DropIndex("dbo.VehicleDrivers", new[] { "DriverID" });
            DropIndex("dbo.DeliveryLocations", new[] { "VehicleDriver_VehicleDriverID" });
            DropIndex("dbo.DeliveryLocations", new[] { "EvacuationCenter_EvacuationCenterID" });
            AlterColumn("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID", c => c.Int(nullable: false));
            AlterColumn("dbo.ReliefGoodVehicles", "PlateNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Drivers", "Gender", c => c.String(nullable: false));
            AlterColumn("dbo.Drivers", "MobileNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Drivers", "MiddleName", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Drivers", "LasttName", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Drivers", "DriverFirstName", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.VehicleDrivers", "ReliefGoodVehicleID", c => c.Int());
            AlterColumn("dbo.VehicleDrivers", "DriverID", c => c.Int());
            AlterColumn("dbo.EvacuationCenters", "EvacDescription", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.EvacuationCenters", "EvacName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID", c => c.Int(nullable: false));
            AlterColumn("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID", c => c.Int(nullable: false));
            AlterColumn("dbo.Categories", "CategoryType", c => c.String(nullable: false, maxLength: 30));
            RenameColumn(table: "dbo.VehicleDrivers", name: "ReliefGoodVehicleID", newName: "PlateNumber_ReliefGoodVehicleID");
            RenameColumn(table: "dbo.VehicleDrivers", name: "DriverID", newName: "DriverName_DriverID");
            CreateIndex("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID");
            CreateIndex("dbo.VehicleDrivers", "PlateNumber_ReliefGoodVehicleID");
            CreateIndex("dbo.VehicleDrivers", "DriverName_DriverID");
            CreateIndex("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID");
            CreateIndex("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID");
            AddForeignKey("dbo.VehicleDrivers", "PlateNumber_ReliefGoodVehicleID", "dbo.ReliefGoodVehicles", "ReliefGoodVehicleID");
            AddForeignKey("dbo.VehicleDrivers", "DriverName_DriverID", "dbo.Drivers", "DriverID");
            AddForeignKey("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID", "dbo.ReliefGoodVehicles", "ReliefGoodVehicleID", cascadeDelete: true);
            AddForeignKey("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID", "dbo.VehicleDrivers", "VehicleDriverID", cascadeDelete: true);
            AddForeignKey("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID", "dbo.EvacuationCenters", "EvacuationCenterID", cascadeDelete: true);
        }
    }
}
