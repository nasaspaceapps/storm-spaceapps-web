namespace STORMV3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryType = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        CityID = c.Int(nullable: false, identity: true),
                        CityName = c.String(),
                        Country = c.String(),
                        Latitude = c.Single(nullable: false),
                        Longitude = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.CityID);
            
            CreateTable(
                "dbo.DeliveryLocations",
                c => new
                    {
                        DeliveryLocationID = c.Int(nullable: false, identity: true),
                        DateOfDispatch = c.DateTime(nullable: false),
                        EstimateDateOfArrival = c.DateTime(nullable: false),
                        EvacuationCenter_EvacuationCenterID = c.Int(nullable: false),
                        VehicleDriver_VehicleDriverID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DeliveryLocationID)
                .ForeignKey("dbo.EvacuationCenters", t => t.EvacuationCenter_EvacuationCenterID, cascadeDelete: true)
                .ForeignKey("dbo.VehicleDrivers", t => t.VehicleDriver_VehicleDriverID, cascadeDelete: true)
                .Index(t => t.EvacuationCenter_EvacuationCenterID)
                .Index(t => t.VehicleDriver_VehicleDriverID);
            
            CreateTable(
                "dbo.EvacuationCenters",
                c => new
                    {
                        EvacuationCenterID = c.Int(nullable: false, identity: true),
                        EvacName = c.String(nullable: false, maxLength: 50),
                        EvacDescription = c.String(nullable: false, maxLength: 100),
                        Longitude = c.Single(nullable: false),
                        Latitude = c.Single(nullable: false),
                        CurrentNumber = c.Int(nullable: false),
                        Capacity = c.Int(nullable: false),
                        Status = c.String(),
                        CategoryType_CategoryID = c.Int(),
                    })
                .PrimaryKey(t => t.EvacuationCenterID)
                .ForeignKey("dbo.Categories", t => t.CategoryType_CategoryID)
                .Index(t => t.CategoryType_CategoryID);
            
            CreateTable(
                "dbo.VehicleDrivers",
                c => new
                    {
                        VehicleDriverID = c.Int(nullable: false, identity: true),
                        DriverName_DriverID = c.Int(),
                        PlateNumber_ReliefGoodVehicleID = c.Int(),
                    })
                .PrimaryKey(t => t.VehicleDriverID)
                .ForeignKey("dbo.Drivers", t => t.DriverName_DriverID)
                .ForeignKey("dbo.ReliefGoodVehicles", t => t.PlateNumber_ReliefGoodVehicleID)
                .Index(t => t.DriverName_DriverID)
                .Index(t => t.PlateNumber_ReliefGoodVehicleID);
            
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        DriverID = c.Int(nullable: false, identity: true),
                        DriverFirstName = c.String(nullable: false, maxLength: 30),
                        LasttName = c.String(nullable: false, maxLength: 30),
                        MiddleName = c.String(nullable: false, maxLength: 30),
                        MobileNumber = c.String(nullable: false),
                        Gender = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.DriverID);
            
            CreateTable(
                "dbo.ReliefGoodVehicles",
                c => new
                    {
                        ReliefGoodVehicleID = c.Int(nullable: false, identity: true),
                        PlateNumber = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ReliefGoodVehicleID);
            
            CreateTable(
                "dbo.FloodAreas",
                c => new
                    {
                        FloodAreaId = c.Int(nullable: false, identity: true),
                        Tittle = c.String(),
                        descripton = c.String(),
                        Year = c.DateTime(nullable: false),
                        FloodLevel = c.String(),
                        City_CityID = c.Int(),
                    })
                .PrimaryKey(t => t.FloodAreaId)
                .ForeignKey("dbo.Cities", t => t.City_CityID)
                .Index(t => t.City_CityID);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationID = c.Int(nullable: false, identity: true),
                        Longitude = c.Single(nullable: false),
                        Latitude = c.Single(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        VehicleDriver_ReliefGoodVehicleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LocationID)
                .ForeignKey("dbo.ReliefGoodVehicles", t => t.VehicleDriver_ReliefGoodVehicleID, cascadeDelete: true)
                .Index(t => t.VehicleDriver_ReliefGoodVehicleID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Locations", "VehicleDriver_ReliefGoodVehicleID", "dbo.ReliefGoodVehicles");
            DropForeignKey("dbo.FloodAreas", "City_CityID", "dbo.Cities");
            DropForeignKey("dbo.DeliveryLocations", "VehicleDriver_VehicleDriverID", "dbo.VehicleDrivers");
            DropForeignKey("dbo.VehicleDrivers", "PlateNumber_ReliefGoodVehicleID", "dbo.ReliefGoodVehicles");
            DropForeignKey("dbo.VehicleDrivers", "DriverName_DriverID", "dbo.Drivers");
            DropForeignKey("dbo.DeliveryLocations", "EvacuationCenter_EvacuationCenterID", "dbo.EvacuationCenters");
            DropForeignKey("dbo.EvacuationCenters", "CategoryType_CategoryID", "dbo.Categories");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Locations", new[] { "VehicleDriver_ReliefGoodVehicleID" });
            DropIndex("dbo.FloodAreas", new[] { "City_CityID" });
            DropIndex("dbo.VehicleDrivers", new[] { "PlateNumber_ReliefGoodVehicleID" });
            DropIndex("dbo.VehicleDrivers", new[] { "DriverName_DriverID" });
            DropIndex("dbo.EvacuationCenters", new[] { "CategoryType_CategoryID" });
            DropIndex("dbo.DeliveryLocations", new[] { "VehicleDriver_VehicleDriverID" });
            DropIndex("dbo.DeliveryLocations", new[] { "EvacuationCenter_EvacuationCenterID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Locations");
            DropTable("dbo.FloodAreas");
            DropTable("dbo.ReliefGoodVehicles");
            DropTable("dbo.Drivers");
            DropTable("dbo.VehicleDrivers");
            DropTable("dbo.EvacuationCenters");
            DropTable("dbo.DeliveryLocations");
            DropTable("dbo.Cities");
            DropTable("dbo.Categories");
        }
    }
}
