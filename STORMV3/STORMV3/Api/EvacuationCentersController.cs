﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using STORMV3.Models;

namespace STORMV3.Api
{
    public class EvacuationCentersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/EvacuationCenters
        public IQueryable<EvacuationCenter> GetEvacuationCenters()
        {
            return db.EvacuationCenters;
        }

        // GET: api/EvacuationCenters/5
        [ResponseType(typeof(EvacuationCenter))]
        public IHttpActionResult GetEvacuationCenter(int id)
        {
            EvacuationCenter evacuationCenter = db.EvacuationCenters.Find(id);
            if (evacuationCenter == null)
            {
                return NotFound();
            }

            return Ok(evacuationCenter);
        }

        // PUT: api/EvacuationCenters/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEvacuationCenter(int id, EvacuationCenter evacuationCenter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != evacuationCenter.EvacuationCenterID)
            {
                return BadRequest();
            }

            db.Entry(evacuationCenter).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EvacuationCenterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EvacuationCenters
        [ResponseType(typeof(EvacuationCenter))]
        public IHttpActionResult PostEvacuationCenter(EvacuationCenter evacuationCenter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EvacuationCenters.Add(evacuationCenter);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = evacuationCenter.EvacuationCenterID }, evacuationCenter);
        }

        // DELETE: api/EvacuationCenters/5
        [ResponseType(typeof(EvacuationCenter))]
        public IHttpActionResult DeleteEvacuationCenter(int id)
        {
            EvacuationCenter evacuationCenter = db.EvacuationCenters.Find(id);
            if (evacuationCenter == null)
            {
                return NotFound();
            }

            db.EvacuationCenters.Remove(evacuationCenter);
            db.SaveChanges();

            return Ok(evacuationCenter);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EvacuationCenterExists(int id)
        {
            return db.EvacuationCenters.Count(e => e.EvacuationCenterID == id) > 0;
        }
    }
}