﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace STORMV3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "The inception of NOBIS QUOD ASTRA (We are the stars), a team of eight highly dedicated and committed team members, occurred in lieu of NASA’s annual hackathon, SpaceApps. As developers, these eight members came together to address a problem that has been the cause of the loss of many lives all around the world – flood. Falling under the Planetary Blues, Water, water everywhere! Category; empowered by the mission of the De La Salle – College of Saint Benilde and NASA, which is to contribute to the betterment of our world, the team has developed STORM: Strategic Tactics for Operational and Relief Measures.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Please contact us for any concerns.";

            return View();
        }
    }
}