﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using STORMV3.Models;

namespace STORMV3.Controllers
{
    public class DeliveryLocationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DeliveryLocations
        public ActionResult Index()
        {
            return View(db.DeliveryLocations.ToList());
        }

        // GET: DeliveryLocations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryLocation deliveryLocation = db.DeliveryLocations.Find(id);
            if (deliveryLocation == null)
            {
                return HttpNotFound();
            }
            return View(deliveryLocation);
        }

        // GET: DeliveryLocations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DeliveryLocations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DeliveryLocationID,DateOfDispatch,EstimateDateOfArrival")] DeliveryLocation deliveryLocation)
        {
            if (ModelState.IsValid)
            {
                db.DeliveryLocations.Add(deliveryLocation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(deliveryLocation);
        }

        // GET: DeliveryLocations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryLocation deliveryLocation = db.DeliveryLocations.Find(id);
            if (deliveryLocation == null)
            {
                return HttpNotFound();
            }
            return View(deliveryLocation);
        }

        // POST: DeliveryLocations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DeliveryLocationID,DateOfDispatch,EstimateDateOfArrival")] DeliveryLocation deliveryLocation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deliveryLocation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(deliveryLocation);
        }

        // GET: DeliveryLocations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryLocation deliveryLocation = db.DeliveryLocations.Find(id);
            if (deliveryLocation == null)
            {
                return HttpNotFound();
            }
            return View(deliveryLocation);
        }

        // POST: DeliveryLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DeliveryLocation deliveryLocation = db.DeliveryLocations.Find(id);
            db.DeliveryLocations.Remove(deliveryLocation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
