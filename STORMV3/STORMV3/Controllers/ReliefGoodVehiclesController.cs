﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using STORMV3.Models;

namespace STORMV3.Controllers
{
    public class ReliefGoodVehiclesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ReliefGoodVehicles
        public ActionResult Index()
        {
            return View(db.ReliefGoodVehicles.ToList());
        }

        // GET: ReliefGoodVehicles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReliefGoodVehicle reliefGoodVehicle = db.ReliefGoodVehicles.Find(id);
            if (reliefGoodVehicle == null)
            {
                return HttpNotFound();
            }
            return View(reliefGoodVehicle);
        }

        // GET: ReliefGoodVehicles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ReliefGoodVehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReliefGoodVehicleID,PlateNumber")] ReliefGoodVehicle reliefGoodVehicle)
        {
            if (ModelState.IsValid)
            {
                db.ReliefGoodVehicles.Add(reliefGoodVehicle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reliefGoodVehicle);
        }

        // GET: ReliefGoodVehicles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReliefGoodVehicle reliefGoodVehicle = db.ReliefGoodVehicles.Find(id);
            if (reliefGoodVehicle == null)
            {
                return HttpNotFound();
            }
            return View(reliefGoodVehicle);
        }

        // POST: ReliefGoodVehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReliefGoodVehicleID,PlateNumber")] ReliefGoodVehicle reliefGoodVehicle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reliefGoodVehicle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reliefGoodVehicle);
        }

        // GET: ReliefGoodVehicles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReliefGoodVehicle reliefGoodVehicle = db.ReliefGoodVehicles.Find(id);
            if (reliefGoodVehicle == null)
            {
                return HttpNotFound();
            }
            return View(reliefGoodVehicle);
        }

        // POST: ReliefGoodVehicles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReliefGoodVehicle reliefGoodVehicle = db.ReliefGoodVehicles.Find(id);
            db.ReliefGoodVehicles.Remove(reliefGoodVehicle);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
