﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using STORMV3.Models;

namespace STORMV3.Controllers
{
    public class VehicleDriversController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: VehicleDrivers
        public ActionResult Index()
        {
            return View(db.VehicleDrivers.ToList());
        }

        // GET: VehicleDrivers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleDriver vehicleDriver = db.VehicleDrivers.Find(id);
            if (vehicleDriver == null)
            {
                return HttpNotFound();
            }
            return View(vehicleDriver);
        }

        // GET: VehicleDrivers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VehicleDrivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VehicleDriverID")] VehicleDriver vehicleDriver)
        {
            if (ModelState.IsValid)
            {
                db.VehicleDrivers.Add(vehicleDriver);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vehicleDriver);
        }

        // GET: VehicleDrivers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleDriver vehicleDriver = db.VehicleDrivers.Find(id);
            if (vehicleDriver == null)
            {
                return HttpNotFound();
            }
            return View(vehicleDriver);
        }

        // POST: VehicleDrivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VehicleDriverID")] VehicleDriver vehicleDriver)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicleDriver).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vehicleDriver);
        }

        // GET: VehicleDrivers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleDriver vehicleDriver = db.VehicleDrivers.Find(id);
            if (vehicleDriver == null)
            {
                return HttpNotFound();
            }
            return View(vehicleDriver);
        }

        // POST: VehicleDrivers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VehicleDriver vehicleDriver = db.VehicleDrivers.Find(id);
            db.VehicleDrivers.Remove(vehicleDriver);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
