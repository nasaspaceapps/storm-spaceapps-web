﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using STORMV3.Models;

namespace STORMV3.Controllers
{
    public class FloodAreasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: FloodAreas
        public ActionResult Index()
        {
            return View(db.FloodAreas.ToList());
        }

        // GET: FloodAreas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FloodArea floodArea = db.FloodAreas.Find(id);
            if (floodArea == null)
            {
                return HttpNotFound();
            }
            return View(floodArea);
        }

        // GET: FloodAreas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FloodAreas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FloodAreaId,Tittle,descripton,Year,FloodLevel")] FloodArea floodArea)
        {
            if (ModelState.IsValid)
            {
                db.FloodAreas.Add(floodArea);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(floodArea);
        }

        // GET: FloodAreas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FloodArea floodArea = db.FloodAreas.Find(id);
            if (floodArea == null)
            {
                return HttpNotFound();
            }
            return View(floodArea);
        }

        // POST: FloodAreas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FloodAreaId,Tittle,descripton,Year,FloodLevel")] FloodArea floodArea)
        {
            if (ModelState.IsValid)
            {
                db.Entry(floodArea).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(floodArea);
        }

        // GET: FloodAreas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FloodArea floodArea = db.FloodAreas.Find(id);
            if (floodArea == null)
            {
                return HttpNotFound();
            }
            return View(floodArea);
        }

        // POST: FloodAreas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FloodArea floodArea = db.FloodAreas.Find(id);
            db.FloodAreas.Remove(floodArea);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
