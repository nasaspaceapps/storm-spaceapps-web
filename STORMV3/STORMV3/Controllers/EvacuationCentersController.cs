﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using STORMV3.Models;

namespace STORMV3.Controllers
{
    public class EvacuationCentersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EvacuationCenters
        public ActionResult Index()
        {
            return View(db.EvacuationCenters.ToList());
        }

        // GET: EvacuationCenters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EvacuationCenter evacuationCenter = db.EvacuationCenters.Find(id);
            if (evacuationCenter == null)
            {
                return HttpNotFound();
            }
            return View(evacuationCenter);
        }

        // GET: EvacuationCenters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EvacuationCenters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EvacuationCenterID,EvacName,EvacDescription,Longitude,Latitude,CurrentNumber,Capacity,Status")] EvacuationCenter evacuationCenter)
        {
            if (ModelState.IsValid)
            {
                db.EvacuationCenters.Add(evacuationCenter);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(evacuationCenter);
        }

        // GET: EvacuationCenters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EvacuationCenter evacuationCenter = db.EvacuationCenters.Find(id);
            if (evacuationCenter == null)
            {
                return HttpNotFound();
            }
            return View(evacuationCenter);
        }

        // POST: EvacuationCenters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EvacuationCenterID,EvacName,EvacDescription,Longitude,Latitude,CurrentNumber,Capacity,Status")] EvacuationCenter evacuationCenter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(evacuationCenter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(evacuationCenter);
        }

        // GET: EvacuationCenters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EvacuationCenter evacuationCenter = db.EvacuationCenters.Find(id);
            if (evacuationCenter == null)
            {
                return HttpNotFound();
            }
            return View(evacuationCenter);
        }

        // POST: EvacuationCenters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EvacuationCenter evacuationCenter = db.EvacuationCenters.Find(id);
            db.EvacuationCenters.Remove(evacuationCenter);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
