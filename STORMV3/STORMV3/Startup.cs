﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(STORMV3.Startup))]
namespace STORMV3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
